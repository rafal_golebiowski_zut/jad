import pandas as pd
import matplotlib.pyplot as plt

autos = pd.read_csv("autos.csv")

from sklearn.datasets import make_moons
[X, y] = make_moons(n_samples=1000, noise=0.5)


from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
clf = AdaBoostClassifier(n_estimators = 100, base_estimator = DecisionTreeClassifier(max_depth=3))
clf.fit(X, y)

import matplotlib.pyplot as plt
import numpy as np
plt.scatter(X[:, 0], X[:, 1], c = y)

x1 = np.linspace(np.min(X[:, 0]), np.max(X[:, 0]), 100)
x2 = np.linspace(np.min(X[:, 1]), np.max(X[:, 1]), 100)
X1Mesh, X2Mesh = np.meshgrid(x1, x2)

xy = np.vstack([X1Mesh.ravel(), X2Mesh.ravel()]).T
YMesh = clf.predict(xy).reshape(X2Mesh.shape)

plt.contour(X1Mesh, X2Mesh, YMesh, levels = 2)

x1 = np.linspace(-1.5, 1.5, 50)
x2 = np.linspace(-1.5, 1.5, 50)
X1Mesh, X2Mesh = np.meshgrid(x1, x2)

YMesh = (X1Mesh**2 + X2Mesh**2 - 1)**3 - X1Mesh**2 * X2Mesh**3
plt.contour(X1Mesh, X2Mesh, YMesh, levels = 2)