# -*- coding: utf-8 -*-

import pandas
import numpy as np
from scipy.io import arff
from scipy import stats
from matplotlib import pyplot as plt

#1
df = pandas.DataFrame({"x": [1, 2, 3, 4, 5], "y": ['a', 'b', 'a', 'b', 'b']})
#2
df.groupby("y").mean()
#3
pandas.value_counts(df["y"])

#4
#a1 = np.loadtxt("autos.csv")
autos = pandas.read_csv("autos.csv")
#a3 = arff.loadarff("autos.arff")

#5
autos.groupby("make")["citympg"].mean()
#6
autos.groupby("make")["fueltype"].count()

autos["price"].mean()
#7
p = np.polyfit(autos["length"], autos["citympg"], 2)
#8
stats.kruskal(autos["length"], autos["citympg"])

#9
x = np.linspace(autos["length"].min(), autos["length"].max(), 100)
y = np.polyval(p, x)

plt.plot(autos["length"], autos["citympg"], "*")
plt.plot(x, y)
plt.figure()
#!0
kde = stats.gaussian_kde(autos["length"])

plt.plot(x, kde.evaluate(x), label = "kde")
plt.plot(autos["length"], np.zeros_like(autos["length"]), "*", label = "length")
plt.legend()
plt.figure()

#11
plt.subplot(1, 2, 1)
plt.plot(x, kde.evaluate(x), label = "kde")
plt.plot(autos["length"], np.zeros_like(autos["length"]), "*", label = "length")
plt.legend()

x2 = np.linspace(autos["width"].min(), autos["width"].max(), 100)
kde2 = stats.gaussian_kde(autos["width"])

plt.subplot(1, 2, 2)
plt.plot(x2, kde2.evaluate(x2), label = "kde")
plt.plot(autos["width"], np.zeros_like(autos["width"]), "*", label = "width")
plt.legend()
plt.figure()

#13
kde3 = stats.gaussian_kde(autos[["length", "width"]].T)
X1, X2 = np.meshgrid(x, x2)

plt.contour(X1, X2, kde3.evaluate(np.array([x, x2]).T))






