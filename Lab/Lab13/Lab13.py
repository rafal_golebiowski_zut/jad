import scipy
import sklearn
import numpy as np
from sklearn import datasets
from sklearn.cluster import AgglomerativeClustering 
from sklearn.cluster import KMeans 
from sklearn.decomposition import PCA
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from scipy.spatial import ConvexHull


iris = datasets.load_iris()

X = iris['data']
d = iris['target']
XPca = PCA(n_components=2).fit_transform(X)


agglomeratives = [AgglomerativeClustering(linkage="single", n_clusters=3),
                AgglomerativeClustering(linkage="average", n_clusters=3),
                AgglomerativeClustering(linkage="complete", n_clusters=3),
                AgglomerativeClustering(linkage="ward", n_clusters=3)]

kMeans = KMeans(3)
kMeans.fit(X)

for agglomerative in agglomeratives:
    agglomerative.fit(X)

    plt.scatter(XPca[:, 0], XPca[:, 1], c = agglomerative.labels_) 
    plt.title(f'AgglomerativeClustering(linkage={agglomerative.linkage})')

    for i in range(3):
        XHull = XPca[agglomerative.labels_ == i, :]
        if(XHull.shape[0] > 2):
            hull = ConvexHull(XHull)
            for simplex in hull.simplices:
                plt.plot(XHull[simplex, 0], XHull[simplex, 1], 'k-')
        elif(XHull.shape[0] == 2):
            plt.plot(XHull[:, 0], XHull[:, 1], 'k-')

    plt.show()

plt.scatter(XPca[:, 0], XPca[:, 1], c = kMeans.labels_) 
plt.title(f'kMeans')
plt.show()


jaccardon = np.sum(np.max(confusion_matrix(d, kMeans.labels_), axis = 1))
print(jaccardon / np.sum(jaccardon))

