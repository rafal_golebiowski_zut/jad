tmp <- factor('c', c('a', 'b', 'c', 'd', 'e'))
tmpInt <- as.integer(tmp)



library(foreign)
autos = read.arff("autos.arff")

isFactor = is.factor(autos$bodystyle)
bodystyleAsInt = as.numeric(autos$bodystyle)
print(bodystyleAsInt[1:5])



tmp <- 10 %/% 3
tmp <- 10 %% 3
tmp <- floor(3.5)
tmp <- ceiling(3.5)
euklides <- function(a, b)
{
  while (b != 0) 
  {
    c <- a %% b
    a <- b
    b <- c
  }
  return(a)
}
euklides(100, 75)


tmp <- (exp(1i*pi) + 1)^(0.05)




library(Matrix)

m <- matrix(c(10, 20, 30, 40, 20, 30, 23, 43, 45, 34, 41, 21), 3, 4)


mZeroOne <- matrix(c(matrix(0, 2, 1), matrix(1, 2, 2)), 2, 3)

m1 <- matrix(1:8, 2, 4, TRUE)
m2 <- matrix(c(1:4, 1:4), 2, 4)
m3 <- matrix(c(1:3, 1:3), 2, 3)

m4 <- diag(5)
m5 <- diag(10, 5)

m6 <- matrix(runif(30, 0, 10), 5, 6)



tmp <- data.frame(wiek = runif(10, 20, 30), plec = factor('M', c('M', 'K')))




zad1 <- function(n)
{
  return(outer(1:n, 1:n, function(x, y) 
    {
      1/(x + y - 1)
  }))
}

z1 <- zad1(5)


zad2 <- function(maxN)
{
  tmp <- zad1(maxN)
  result = 1:maxN
  for (index in 2:maxN)
  {
    A = tmp[1:index, 1:index]
    if (is.matrix(A)) 
    {
      result[index] = condest(A)$est
    }
  }
  return(result)
}

z2 <- zad2(5)



zad3Recurrence <- function(n)
{
  if(n == 0)
    return(1)
  return(zad3Recurrence(n - 1) * n)
}
zad3Iter <- function(n)
{
  tmp <- 1
  for (index in 1:n)
  {
    tmp <- tmp * index
  }
  return(tmp)
}

z3R <- zad3Recurrence(5)
z3I <- zad3Iter(5)
z3G <- gamma(5 + 1)


zad4 <- function(n, k)
{
  return(zad3Recurrence(n) /
           (zad3Recurrence(k) * zad3Recurrence(n - k)))
}

z4 <- zad4(5, 1)


zad5 <- function()
{
  p = outer(seq(-2, 2, by=0.1), seq(-2, 2, by=0.1), function(x, y) 
  {
    x+y*1i
  })
  mask = mand(10, p);
  
  image(mask)
}

mand <- function(n, p)
{
  xSize = ncol(p)
  ySize = nrow(p)
  mask = matrix(0, ySize, xSize)
  z = p;
  for (tmp in 1:n) 
  {
    z <- z^2 + p
    
    for (row in 1:ySize) 
    {
      for (col in 1:xSize) 
      {
        if(mask[row, col] == 0 && abs(z[row, col]) > 2)
        {
          mask[row, col] = tmp;
        }
      }
    }
  }
  return(mask)
}

z5 <- zad5()

x = seq(-3, 3, by=0.1)
y = exp(x)
plot(x, y)
legend(-3, 20, "exp")

x = seq(-pi, pi, by = 0.1)
y = sin(x)
plot(x, y)
legend(-pi, 1, "sin")

x = seq(0, 1, by = 0.01)
y = x * log(x)
plot(x, y)
legend(0, 0, "xlog(x)")


x = seq(-10, 10, by = 0.1)
y = sin(pi * x) / (pi * x)
plot(x, y)
legend(-10, 1, "sin(pi * x) / (pi * x)")

n = 20
p = 0.4
k = 0:n
y = k
for (index in k) 
{
  y[index + 1] = zad4(n, index) * p^index * (1 - p)^(n - index)
  
}
plot(k, y)

