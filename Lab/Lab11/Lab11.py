import pandas as pd
import numpy as np
from scipy.io import arff
from scipy import stats
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn import naive_bayes
from sklearn import discriminant_analysis
from sklearn import svm
from sklearn import tree
from sklearn import metrics
from matplotlib import pyplot as plt



def main():
    X, y = make_classification(n_samples=1000,
                    n_features=2, n_informative=2, 
                    n_redundant=0, n_repeated=0, 
                    n_classes=2, n_clusters_per_class=2)
    plt.scatter(X[: , 0], X[:, 1], c = y)
    plt.show()


    classificators = [naive_bayes.GaussianNB(),
                discriminant_analysis.QuadraticDiscriminantAnalysis(),
                svm.SVC(probability=True, gamma="auto"),
                tree.DecisionTreeClassifier()]

    

    

    df = pd.DataFrame(columns=["Classificator", 
                        "Accuracy",
                        "Recall",
                        "Precision",
                        "F1",
                        "ROC"])
    loops = 2
    classificatorsCount = len(classificators)
    for i in range(loops):
        XTrain, XTest, yTrain, yTest = train_test_split(X, y, test_size=0.5, random_state=i)
        
        for j, classificator in enumerate(classificators):
            classificator.fit(XTrain, yTrain)
            yPredicted = classificator.predict(XTest)

            row = [classificator.__class__.__name__]
            row.append(metrics.accuracy_score(yTest, yPredicted))
            row.append(metrics.recall_score(yTest, yPredicted))
            row.append(metrics.precision_score(yTest, yPredicted))
            row.append(metrics.f1_score(yTest, yPredicted))
            row.append(metrics.roc_auc_score(yTest, yPredicted))
            df.loc[i * classificatorsCount + j] = row

            
            if(i == loops - 1):
                prPred = classificator.predict_proba(XTest)
                auc = metrics.roc_auc_score(yTest, prPred[:, 1])

                fpr, tpr, thresholds = metrics.roc_curve(yTest, prPred[:, 1])

                plt.plot(fpr, tpr)
                plt.show()



    print(df.groupby("Classificator").mean())










main()