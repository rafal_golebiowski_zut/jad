X, Y = make_classification(n_samples=1000, n_features=2, n_informative=2, n_redundant=0, n_repeated=0, n_classes=2, n_clusters_per_class=2)
clf_list = [GaussianNB(), QuadraticDiscriminantAnalysis(), KNeighborsClassifier(), SVC(), DecisionTreeClassifier(max_depth=2)]
for i, clf in enumerate(clf_list):
    plt.figure()
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)
    plt.scatter(X[:, 0], X[:, 1], c=Y)
    clf.fit(X_train, Y_train)
    XX, YY = np.meshgrid(np.linspace(np.min(X[:, 0]), np.max(X[:, 0]), 100), np.linspace(np.min(X[:, 1]), np.max(X[:, 1]), 100))
    Z = clf.predict(np.vstack([XX.ravel(), YY.ravel()]).T)
    plt.contour(XX, YY, np.reshape(Z, (100, 100)))
    plt.show()