import pandas as pd
import numpy as np
from scipy.io import arff
from scipy import stats
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn import naive_bayes
from sklearn import discriminant_analysis
from sklearn import svm
from sklearn import tree
from sklearn import metrics
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from time import time



def main():
    X, y = make_classification(n_samples=1000,
                    n_features=2, n_informative=2, 
                    n_redundant=0, n_repeated=0, 
                    n_classes=2, n_clusters_per_class=2)
    plt.scatter(X[: , 0], X[:, 1], c = y)
    plt.show()


    classificators = [naive_bayes.GaussianNB(),
                discriminant_analysis.QuadraticDiscriminantAnalysis(),
                svm.SVC(probability=True, gamma="auto"),
                tree.DecisionTreeClassifier()]

    params = {}
    params[classificators[0]] = {'var_smoothing': np.logspace(-5, 5, 100)}
    params[classificators[1]] = {'reg_param': np.logspace(-15, 1, 10),
                                'tol': np.logspace(-15, 1, 10)}
    params[classificators[2]] = {'kernel': ('linear', 'rbf'),
                                'C': range(1, 10)}
    params[classificators[3]] = {'criterion': ["gini", "entropy"],
                                'max_depth': range(5, 50, 5)}


    gss =[]
    for j, classificator in enumerate(classificators):
        gs = GridSearchCV(classificator, params[classificator], 'accuracy', cv = 5)
        gs.fit(X, y)

        classificator.set_params(**gs.best_params_)
        print(gs.best_params_)

        gss.append(gs)

    plt.semilogx(np.logspace(-5, 5, 100), gss[0].cv_results_['mean_test_score'])
    plt.title("naive_bayes.GaussianNB(var_smoothing)")
    plt.show()

    plt.plot(range(1, 10), gss[2].cv_results_['mean_test_score'][::2])
    plt.plot(range(1, 10), gss[2].cv_results_['mean_test_score'][1::2])
    plt.title("svm.SVC(C)")
    plt.legend(['linear', 'rbf'])
    plt.show()


    fig = plt.figure()
    ax = fig.gca(projection='3d')

    XX = np.logspace(-15, 1, 10)
    YY = np.logspace(-15, 1, 10)
    XX, YY = np.meshgrid(XX, YY)

    # Plot the surface.
    ax.plot_surface(np.log10(XX), np.log10(YY), gss[1].cv_results_['mean_test_score'].reshape((10, 10)))
    ax.set_xlabel('tol')
    ax.set_ylabel('reg_param')
    plt.title("discriminant_analysis.QuadraticDiscriminantAnalysis")
    plt.show()


    plt.plot(range(5, 50, 5), gss[3].cv_results_['mean_test_score'][::2])
    plt.plot(range(5, 50, 5), gss[3].cv_results_['mean_test_score'][1::2])
    plt.title("tree.DecisionTreeClassifier(max_depth)")
    plt.legend(['gini', 'entropy'])
    plt.show()



    df = pd.DataFrame(columns=["Classificator", 
                        "Accuracy",
                        "Recall",
                        "Precision",
                        "F1",
                        "ROC",
                        "trainTime",
                        "testTime"])
    
    loops = 100
    classificatorsCount = len(classificators)

    fprs = []
    tprs = []

    for i in range(loops):
        XTrain, XTest, yTrain, yTest = train_test_split(X, y, test_size=0.5, random_state=i)
        
        for j, classificator in enumerate(classificators):
            trainTime = time()
            classificator.fit(XTrain, yTrain)
            trainTime = time() - trainTime
            testTime = time()
            yPredicted = classificator.predict(XTest)
            testTime = time() - testTime

            row = [classificator.__class__.__name__]
            row.append(metrics.accuracy_score(yTest, yPredicted))
            row.append(metrics.recall_score(yTest, yPredicted))
            row.append(metrics.precision_score(yTest, yPredicted))
            row.append(metrics.f1_score(yTest, yPredicted))
            row.append(metrics.roc_auc_score(yTest, yPredicted))
            row.append(trainTime)
            row.append(testTime)
            df.loc[i * classificatorsCount + j] = row

            
            if(i == loops - 1):
                prPred = classificator.predict_proba(XTest)
                auc = metrics.roc_auc_score(yTest, prPred[:, 1])

                fpr, tpr, thresholds = metrics.roc_curve(yTest, prPred[:, 1])

                fprs.append(fpr)
                tprs.append(tpr)

                plt.scatter(X[: , 0], X[:, 1], c = y)
                XX, YY = np.meshgrid(np.linspace(np.min(X[:, 0]), np.max(X[:, 0]), 100), np.linspace(np.min(X[:, 1]), np.max(X[:, 1]), 100))
                Z = classificator.predict(np.vstack([XX.ravel(), YY.ravel()]).T)
                plt.contour(XX, YY, np.reshape(Z, (100, 100)))
                plt.title(classificator.__class__.__name__)
                plt.show()


    for j in range(classificatorsCount):
        plt.plot(fprs[j], tprs[j])
    plt.legend(['naive_bayes.GaussianNB',
                'discriminant_analysis.QuadraticDiscriminantAnalysis',
                'svm.SVC',
                'tree.DecisionTreeClassifier'])
    plt.show()
    print(df.groupby("Classificator").mean())










main()