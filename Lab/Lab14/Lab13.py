import scipy
import sklearn
import numpy as np
from sklearn import datasets
from sklearn.cluster import AgglomerativeClustering 
from sklearn.cluster import KMeans 
from sklearn.cluster import DBSCAN
from sklearn.decomposition import PCA
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from scipy.spatial import ConvexHull
from sklearn.metrics import jaccard_score
import pandas as pd


iris = datasets.load_iris()

X = iris['data']
d = iris['target']


agglomeratives = [AgglomerativeClustering(linkage="single", n_clusters=3),
                AgglomerativeClustering(linkage="average", n_clusters=3),
                AgglomerativeClustering(linkage="complete", n_clusters=3),
                AgglomerativeClustering(linkage="ward", n_clusters=3)]
for agglomerative in agglomeratives:
    agglomerative.fit(X)

kMeans = KMeans(3)
kMeans.fit(X)

dbscan = DBSCAN()
dbscan.fit(X)

titles = [f'AgglomerativeClustering(linkage={agglomerative.linkage})' for agglomerative in agglomeratives]
titles.append('kMeans')
titles.append('dbscan')
jaccardDf = pd.DataFrame(columns=["Clustering", "JaccardScore", "JaccardManual"])
for k, clustering in enumerate([*agglomeratives, kMeans, dbscan]):
    tmp = confusion_matrix(d, clustering.labels_)
    jaccardDf.loc[k] = [titles[k],
                        jaccard_score(d, clustering.labels_), 
                        np.sum(np.max(tmp, axis = 1))/np.sum(tmp)]
print(jaccardDf)

XPca = PCA(n_components=2).fit_transform(X)
for k, clustering in enumerate([*agglomeratives, kMeans, dbscan]):
    plt.scatter(XPca[:, 0], XPca[:, 1], c = clustering.labels_) 
    plt.title(titles[k])

    for i in range(3):
        XHull = XPca[clustering.labels_ == i, :]
        if(XHull.shape[0] > 2):
            hull = ConvexHull(XHull)
            for simplex in hull.simplices:
                plt.plot(XHull[simplex, 0], XHull[simplex, 1], 'k-')
        elif(XHull.shape[0] == 2):
            plt.plot(XHull[:, 0], XHull[:, 1], 'k-')

    plt.show()

