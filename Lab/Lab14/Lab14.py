import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans 
from skimage.measure import compare_ssim as ssim
from skimage import io
from skimage import data, img_as_float

img  = io.imread('img2.jpeg')

imgReshaped = img.reshape((img.shape[0] * img.shape[1], img.shape[2]))

numberOfCenters = [2, 3, 5, 10, 30, 100]
for i, n in enumerate(numberOfCenters):
    kMeans = KMeans(n, n_init=1)
    kMeans.fit(imgReshaped)
    imgKMeans = kMeans.cluster_centers_[kMeans.labels_].astype(int)

    imgKMeans = imgKMeans.reshape((img.shape[0], img.shape[1], img.shape[2]))

    ssim_const = ssim(img, imgKMeans, multichannel=True)
    ax = plt.subplot(3, 3, i + 2)
    ax.set_title(f'n = {n}, ssim = {ssim(img, imgKMeans, multichannel=True):.5f}')
    ax.imshow(imgKMeans)
    

    
plt.subplot(3, 3, 1)
plt.imshow(img)
plt.show()