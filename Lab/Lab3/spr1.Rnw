\documentclass[11pt,a4paper]{article}
\usepackage{fullpage}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb} 
\usepackage{polski}
\usepackage{graphicx}

\title{Języki analizy danych - sprawozdanie 1}
\author{Marcin Korzeń}
\begin{document}
\SweaveOpts{concordance=TRUE}
\maketitle

\section{Wczytywanie danych}

<< echo=TRUE >>=
library(foreign)
autos<-read.arff("autos.arff")
@

\section{Liczba rekordów i atrybut�w}
<< echo = TRUE >>=
recordsCount = length(autos[,1])
attributesCount = length(autos)
  
sprintf("Liczba rekord�w: %d", recordsCount)
sprintf("Liczba atrybut�w: %d", attributesCount)
@



\end{document}
